import json as lib_json

import yaml as lib_yaml


def json(path):
    with open(path, 'r') as f:
        data = lib_json.load(f)
    return data


def yaml(path):
    with open(path, 'r') as f:
        data = lib_yaml.load(f)
    return data


def df(path):
    import pandas as pd
    return pd.DataFrame.from_dict(yaml(path))
