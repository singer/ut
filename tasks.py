import hashlib
import os
import warnings
from os.path import join

import luigi
from luigi.task import flatten

import etc
from ut import mkdirs


class StartDoneMixin(object):
    WORK_PATH = 'work'

    def get_base_path(self):
        return mkdirs(join(etc.PAR_DIR, self.WORK_PATH, self.__class__.__name__))

    def get_tmp_path(self):
        return mkdirs(join(self.get_base_path(), 'tmp_%s' % os.path.basename(self.output().path)))

    def lstart(self):
        import structlog
        logger = structlog.get_logger(self.__module__)
        self.log = logger
        if isinstance(self.input(), list):
            input_path = [i.path for i in self.input()]
        elif isinstance(self.input(), dict):
            input_path = ["{k} = pd.read_json('{i.path}')".format(k=k, i=i) for k, i in self.input().items()]
        else:
            input_path = self.input().path
        info_line = '  [ START ] %s  ' % self.__class__.__name__
        logger.info(info_line.center(78, '='),
                    inputs=input_path,
                    params=self.to_str_params(only_significant=True)
                    )
        return logger.bind(task=self)

    def ldone(self, extra=''):
        import structlog
        logger = structlog.get_logger(self.__module__)
        if isinstance(self.input(), list):
            input_path = [i.path for i in self.input()]
        elif isinstance(self.input(), dict):
            input_path = ["{k} = pd.read_json('{i.path}')".format(k=k, i=i) for k, i in self.input().items()]
        else:
            input_path = self.input().path
        if isinstance(self.output(), list):
            output_path = [i.path for i in self.input()]
        elif isinstance(self.output(), dict):
            output_path = {k: i.path for k, i in self.output().items()}
        else:
            output_path = "res_df = pd.read_json('{i.path}')".format(i=self.output())
        info_line = ' [%s DONE ] %s ' % (extra, self.__class__.__name__)
        logger.info(info_line.center(78, '='),
                    outputs=output_path,
                    inputs=input_path,
                    params=self.to_str_params(only_significant=True)
                    )


class DataDependentTask(luigi.Task, StartDoneMixin):
    code_version = luigi.Parameter(default=etc.CODE_VERSION)
    rerun = luigi.Parameter(default='')

    def complete(self):
        if self.rerun:
            return False
        inputs = luigi.task.flatten(self.input())
        if not all(map(lambda inpt: inpt.exists(), inputs)):
            return False
        outputs = luigi.task.flatten(self.output())
        if len(outputs) == 0:
            luigi.task.warnings.warn(
                "Task %r without outputs has no custom complete() method" % self,
                stacklevel=2
            )
            return False
        completed = all(map(lambda output: output.exists(), outputs))
        if completed:
            self.ldone(extra=' ALREADY')
        return completed

    def output(self):
        my_hash = self.task_id
        inputs = luigi.task.flatten(self.input())
        in_hash = ''
        for ipt in inputs:
            if ipt.exists() and os.path.isfile(ipt.path):
                in_hash = in_hash + hashlib.md5(open(ipt.path, 'rb').read()).hexdigest()
                in_hash = hashlib.md5(in_hash).hexdigest()
        fname = '{my_hash}-{in_hash}.json'.format(my_hash=my_hash, in_hash=in_hash)
        path = join(self.get_base_path(), fname)
        return luigi.LocalTarget(path)


class LongDataDependentTask(DataDependentTask):
    code_version = luigi.Parameter(default=etc.LONG_CODE_VERSION)

    def complete(self):
        """
        If the task has any outputs, return ``True`` if all outputs exist.
        Otherwise, return ``False``.

        However, you may freely override this method with custom logic.
        """
        outputs = flatten(self.output())
        if len(outputs) == 0:
            warnings.warn(
                "Task %r without outputs has no custom complete() method" % self,
                stacklevel=2
            )
            return False

        return all(map(lambda output: output.exists(), outputs))


    def output(self):
        my_hash = self.task_id
        fname = '{my_hash}.json'.format(my_hash=my_hash)
        path = join(self.get_base_path(), fname)
        return luigi.LocalTarget(path)
