import sys
import threading

import six
from structlog._loggers import WRITE_LOCKS
from structlog._utils import until_not_interrupted


class YamlRenderer(object):
    def __call__(self, logger, name, event_dict):
        import yaml, json
        d = json.loads(event_dict)
        event_keys = ('event', 'level', 'timestamp', 'logger', 'exception')
        res = {k.upper(): d[k] for k in event_keys if k in d}
        res['data'] = {k: d[k] for k in d if k not in event_keys}
        # }
        return yaml.safe_dump(res,
                              width=1000,
                              allow_unicode=True,
                              default_flow_style=False)


def _figure_out_exc_info(v):
    """
    Depending on the Python version will try to do the smartest thing possible
    to transform *v* into an ``exc_info`` tuple.

    :rtype: tuple
    """
    if v is True:
        return sys.exc_info()
    elif six.PY3 and isinstance(v, BaseException):
        return (v.__class__, v, getattr(v, "__traceback__"))

    return v


def format_exc_info(logger, name, event_dict):
    """
    Replace an `exc_info` field by an `exception` string field:

    If *event_dict* contains the key ``exc_info``, there are two possible
    behaviors:

    - If the value is a tuple, render it into the key ``exception``.
    - If the value is an Exception *and* you're running Python 3, render it
      into the key ``exception``.
    - If the value true but no tuple, obtain exc_info ourselves and render
      that.

    If there is no ``exc_info`` key, the *event_dict* is not touched.
    This behavior is analogue to the one of the stdlib's logging.
    """
    exc_info = event_dict.pop('exc_info', None)
    if exc_info:
        exc = _figure_out_exc_info(exc_info)
        exc = list(exc)
        if exc[2]:
            import traceback
            # exc[2] = traceback.extract_tb(exc[2])
            formated_tb = list()
            for f, lineno, mod, code in traceback.extract_tb(exc[2]):
                s = """File "{f}", line {lineno}, in {mod}""".format(f=f, lineno=lineno, mod=mod)
                formated_tb.append({s: [code]})
            exc[2] = formated_tb
        event_dict['exception'] = exc
    return event_dict


class PrintLogger(object):
    """
    Print events into a file.

    :param file file: File to print to. (default: stdout)

    >>> from structlog import PrintLogger
    >>> PrintLogger().msg('hello')
    hello

    Useful if you just capture your stdout with tools like `runit
    <http://smarden.org/runit/>`_ or if you `forward your stderr to syslog
    <https://hynek.me/articles/taking-some-pain-out-of-python-logging/>`_.

    Also very useful for testing and examples since logging is finicky in
    doctests.
    """

    def __init__(self, file=None, name='__main__'):
        self._file = file or sys.stdout
        self._write = self._file.write
        self._flush = self._file.flush
        self.name = name

        lock = WRITE_LOCKS.get(self._file)
        if lock is None:
            lock = threading.Lock()
            WRITE_LOCKS[self._file] = lock
        self._lock = lock

    def __repr__(self):
        return '<PrintLogger(file={0!r})>'.format(self._file)

    def msg(self, message):
        """
        Print *message*.
        """
        with self._lock:
            until_not_interrupted(self._write, message + '\n')
            until_not_interrupted(self._flush)

    log = debug = info = warn = warning = msg
    failure = err = error = critical = exception = msg


class PrintLoggerFactory(object):
    """
    Produce :class:`PrintLogger`\ s.

    To be used with :func:`structlog.configure`\ 's `logger_factory`.

    :param file file: File to print to. (default: stdout)

    Positional arguments are silently ignored.

    .. versionadded:: 0.4.0
    """

    def __init__(self, file=None):
        self._file = file

    def __call__(self, *args):
        name = '__main__'
        if len(args):
            name = args[0]
        return PrintLogger(self._file, name)
