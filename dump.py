import yaml as lib_yaml
from pandas.io import json as lib_json


def json(data, path):
    to_dict_f = getattr(data, "to_dict", None)
    if callable(to_dict_f):
        data = to_dict_f()
    with open(path, 'w') as f:
        f.write(lib_json.dumps(data))

        # lib_json.to_json(path, data)


def yaml(data, path, default_flow_style=False):
    to_dict_f = getattr(data, "to_dict", None)
    if callable(to_dict_f):
        data = to_dict_f()
    with open(path, 'w') as f:
        lib_yaml.safe_dump(lib_json.loads(lib_json.dumps(data)),
                           f,
                           allow_unicode=True,
                           default_flow_style=default_flow_style,
                           indent=4)
