# -*- coding: utf-8 -*-
import os
import re

NOT_ALLOWED_CHARS = re.compile(ur'[^A-Za-zА-Яа-я0-9\s:\.\,\-\\\/\(\)\+]', re.UNICODE)
import string


def make_atom_re(pat):
    pat = re.escape(pat)
    return u'(?:^|\s){pat}(?:$|\s)'.format(pat=pat)


def re_del(pat, s):
    return re.sub(pat, ' ', s, flags=re.IGNORECASE | re.UNICODE)


def sdel(pat, s):
    return re_del(make_atom_re(pat), s).strip()


def sfind(pat, s, do_assert=True):
    pat = make_atom_re(pat=pat)
    matches = re.findall(pat, s, flags=re.IGNORECASE | re.UNICODE)
    results = list()
    if matches:
        if do_assert:
            assert len(matches) == 1
        return matches[0].strip()
        # for match in matches:
        #     results.append(match.strip())
    return results


def rejoin(s):
    s = clean_printable_multilang(s)
    return ' '.join(filter(None, re.split('\s', s)))


def clean_printable(s):
    if s:
        s = filter(lambda x: x in string.printable, s)
        return s
    return ''


def clean_printable_multilang(s):
    return NOT_ALLOWED_CHARS.sub('', s)


def mkdirs(path):
    import os
    if not os.path.exists(path):
        os.makedirs(path)
    return path


def scan_dict(what, where):
    from unidecode import unidecode
    results = list()
    if not what:
        return results
    if not unidecode(clean_printable_multilang(what)):
        return results
    for k, v in sorted(where.items(), key=lambda x: -1 * len(x[1])):
        if unidecode(v.lower()) in unidecode(clean_printable_multilang(what.lower())):
            results.append(k)
    return results


def srem(patterns, s):
    import re
    for pat in patterns:
        s = re.sub(pat, '', s)
    return s.strip()


def download_file(url, out):
    import requests
    r = requests.get(url, stream=True)
    with open(out, 'wb') as f:
        for i, chunk in enumerate(r.iter_content(chunk_size=1024)):
            # log_debug('fetching chunk %s len: %s' % (i, len(chunk)))
            if chunk:  # filter out keep-alive new chunks
                f.write(chunk)
    return out


def clean_float(x):
    import string
    x = x.replace(',', '.')
    x = filter(lambda s: s in string.digits + '.', x)
    return float(x.strip('.'))


def is_number(x):
    try:
        float(x)
    except ValueError:
        return False
    return True


def case_insensetive_split(pat, s):
    s = re.sub(pat, pat, s, flags=re.IGNORECASE).strip()
    return s.split(pat)


def naive_now(_):
    import time
    from datetime import datetime
    ts = time.time()
    dt = datetime.fromtimestamp(ts)
    return '%s+00:00' % str(dt)